<?php

class auth_login extends CI_Model
{
  public function checkLogin($username,$password)
  {
    $this->db->select('*');
    $this->db->where(array('username' => $username,'password' => $password));
    $this->db->from('users');

    $query = $this->db->get();

    if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row;
    } else {
            return false;
    }

  }
}
